﻿using InsertXMLToDB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace InsertXMLToDB
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument xcod = new XmlDocument();
            xcod.Load(@"Xmls/29.04.2020.xml");

            XmlElement root = xcod.DocumentElement;
            //empty list creation for products from xml
            List<Products> productsfromXml = new List<Products>();

            foreach (XmlNode item in root.ChildNodes)
            {
                Products product = new Products();
                product.Sku = item.SelectSingleNode("Sku").InnerText;
                product.Count = int.Parse(item.SelectSingleNode("Count").InnerText);
                product.Name = item.SelectSingleNode("Name").InnerText;
                product.Price = decimal.Parse(item.SelectSingleNode("Price").InnerText);
                productsfromXml.Add(product);
            }

            using (SqlConnection con = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog = Market; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False"))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand("INSERT INTO Products(Count, Price, Name, Sku) VALUES(@Count, @Price, @Name, @Sku)", con))
                    {
                        foreach (var item in productsfromXml)
                        {
                            command.Parameters.Clear();
                            command.Parameters.Add(new SqlParameter("@Count", item.Count));
                            command.Parameters.Add(new SqlParameter("@Name", item.Name));
                            command.Parameters.Add(new SqlParameter("@Price", item.Price));
                            command.Parameters.Add(new SqlParameter("@Sku", item.Sku));
                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
