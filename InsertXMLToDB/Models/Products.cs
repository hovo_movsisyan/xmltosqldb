﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertXMLToDB.Models
{
    public class Products
    {
        public int Id { get; set; } 
        public int Count { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
    }
}
