﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using XML.Models;

namespace XML
{
    class Program
    {
        static void Main(string[] args)
        {
            //xml load
            XmlDocument xcod = new XmlDocument();
            xcod.Load(@"Xmls/29.04.2020.xml");

            XmlElement root = xcod.DocumentElement;
            //empty list creation for products from xml
            List<Product> productsfromXml = new List<Product>();

            foreach (XmlNode item in root.ChildNodes)
            {
                //init singl product and add to productlist
                Product product = new Product();
                product.Sku = item.SelectSingleNode("Sku").InnerText;
                product.Count = int.Parse(item.SelectSingleNode("Count").InnerText);
                product.Name = item.SelectSingleNode("Name").InnerText;
                product.Price = decimal.Parse(item.SelectSingleNode("Price").InnerText);
                decimal.TryParse(item.SelectSingleNode("OldPrice").InnerText, out decimal oldPrice);
                productsfromXml.Add(product);
            }
            Console.WriteLine($"product count from xml --{productsfromXml.Count}");
            //creat empty list for db products
            List<Product> productsfromDb = new List<Product>();
            string ConnString = "Data Source=213.136.89.64, 1433;Initial Catalog=BonusMarket;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=strongpass123.";
            string SqlString = "SELECT * FROM [BonusMarket].[dbo].[Products] where [Count]>0";
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(SqlString, conn))
                {

                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //add evry singl product to list
                            productsfromDb.Add(new Product { Sku = reader["Sku"].ToString() });
                        }
                    }
                }
            }
            var res = productsfromDb.GroupBy(p => p.Sku).Where(p => p.Count() > 1)
                                                    .Select(p => p.Key).ToList();
            foreach (Product item in productsfromXml)
            {
                productsfromDb.RemoveAll(p => p.Sku == item.Sku);
            }
            foreach (Product item in productsfromDb)
            {
                Console.WriteLine($"{item.Sku}");
            }
            Console.WriteLine($"products from Db  {productsfromDb.Count}");
            Console.ReadKey();
        }
    }
}
